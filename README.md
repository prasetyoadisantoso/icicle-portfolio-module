<p align="center"><img src="https://gitlab.com/prasetyo.element/icicle-cms-module/-/raw/master/Icicle%20Module.png" width="150"></p>


# Portfolio Module for ICICLE Admin Panel

This module was build for Portfolio Management System. You can plug-in/plug-out this Module, without worrying destroy the panel.


## Prerequisite Installation
Install the  [ICICLE Administrator Panel](https://gitlab.com/prasetyo.element/laravel-administrator-with-multi-theme).

## How to Install
1. place or extract this module to directory `your_project_directory/Modules` and rename it to <code>Portfolio</code>
2. `php artisan module:enable Portfolio`
3. `php artisan module:migrate` <- Only create table
4. `php artisan module:migrate --seed` <- Create table and its permissions

## How to remove
1. `php artisan module:disable Portfolio`
1. `php artisan module:delete Portfolio`

**Note** :  Permissions not activated, you must manually setting the permissions. But, by default, the module was operated flawlessly

If you want to try this demo, [Click Here!](https://icicle-project.elementdeveloper.com/).

## License

ICICLE Administrator Panel is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

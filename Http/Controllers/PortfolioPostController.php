<?php

namespace Modules\Portfolio\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Portfolio\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Administrator\Classes\ReadModuleClass;
use Modules\Portfolio\Entities\PortfolioCategories;
use Modules\Portfolio\Entities\PortfolioPosts;

class PortfolioPostController extends Controller
{

    // roles permissions variable
    public $roles;

    // Read Modules variable
    public $modules;

    public function __construct()
    {
        /* Permissions CRUD for Portfolio */
        $this->middleware('permission:portfolio-list')->only(['index']);
        $this->middleware('permission:portfolio-create')->only(['create']);
        $this->middleware('permission:portfolio-edit')->only(['edit']);
        $this->middleware('permission:portfolio-delete')->only(['destroy']);


        $this->middleware(['auth', 'verified']);
        $this->roles = Auth::user();
        $this->modules = ReadModuleClass::read();
    }






    /**
     * Display a listing of portfolio.
     * @return Renderable
     */
    public function index(Request $request)
    {
        /* Get Categories data and show it */
        $data = PortfolioPosts::orderBy('id', 'DESC')->paginate(5);

        /* Render to blade */
        return view('portfolio::portfolio.index', compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);
    }







    /**
     * Show the form for creating a portfolio.
     * @return Renderable
     */
    public function create()
    {
        /* Get Categories for Potfolio */
        $category = PortfolioCategories::all();

        /* Render to blade */
        return view('portfolio::portfolio.create', compact('category'));
    }







    /**
     * Store a portfolio in storage and database.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        /* Validating request */
        $this->validate($request, [
            'title' => 'required',
            'categories' => 'required',
            'content' => 'required',
            'image_1' => 'nullable',
            'image_1.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image_2' => 'nullable',
            'image_2.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image_3' => 'nullable',
            'image_3.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image_4' => 'nullable',
            'image_4.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image_5' => 'nullable',
            'image_5.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        /**
         * Check request has file or image
         */
        if ($request->hasFile('image_1')) {

            foreach ($request->file('image_1') as $image) {
                $names = $image->getClientOriginalName();
                $image->move(public_path() . '/uploads/', $names);
                $image1 = $names;
            }
        }

        if ($request->hasFile('image_2')) {

            foreach ($request->file('image_2') as $image) {
                $names = $image->getClientOriginalName();
                $image->move(public_path() . '/uploads/', $names);
                $image2 = $names;
            }
        }

        if ($request->hasFile('image_3')) {

            foreach ($request->file('image_3') as $image) {
                $names = $image->getClientOriginalName();
                $image->move(public_path() . '/uploads/', $names);
                $image3 = $names;
            }
        }

        if ($request->hasFile('image_4')) {

            foreach ($request->file('image_4') as $image) {
                $names = $image->getClientOriginalName();
                $image->move(public_path() . '/uploads/', $names);
                $image4 = $names;
            }
        }

        if ($request->hasFile('image_5')) {

            foreach ($request->file('image_5') as $image) {
                $names = $image->getClientOriginalName();
                $image->move(public_path() . '/uploads/', $names);
                $image5 = $names;
            }
        }



        /* Save Request */
        $data = new PortfolioPosts();

        $data->title = $request->input('title');
        $data->content = $request->input('content');


        /**
         * Check image if exist then saving it
         * */
        if (isset($image1)) {
            $data->image_1 = $image1;
        } else {
            # code...
        }

        if (isset($image2)) {
            $data->image_2 = $image2;
        } else {
            # code...
        }

        if (isset($image3)) {
            $data->image_3 = $image3;
        } else {
            # code...
        }

        if (isset($image4)) {
            $data->image_4 = $image4;
        } else {
            # code...
        }

        if (isset($image5)) {
            $data->image_5 = $image5;
        } else {
            # code...
        }


        $data->save();

        /* Insert Category with POrtfolio */
        $latest =  PortfolioPosts::latest()->first();
        $latest->portcats()->attach($request->categories);

        /* Redirect to portfolio index */
        return redirect()->route('portfolioposts.index')
            ->with('success', 'Portfolio created successfully');
    }









    /**
     * Show the specified resource. Maybe next feature
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('portfolio::show');
    }










    /**
     * Show the form for editing the portfolio.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        /* Get Post with Category relation */
        $data =  PortfolioPosts::with('portcats')->findOrFail($id);
        $categories =  PortfolioCategories::all();

        /* Store category to array */
        foreach ($data->portcats as $key => $value) {
            $category[] = $value->id;
        }

        /* Render to blade */
        return view('portfolio::portfolio.edit', compact('data', 'categories', 'category'));
    }










    /**
     * Update the portfolio in storage and database.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        /* Validating request */
        $this->validate($request, [
            'title' => 'required',
            'categories' => 'required',
            'content' => 'required',
            'image_1' => 'nullable',
            'image_1.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image_2' => 'nullable',
            'image_2.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image_3' => 'nullable',
            'image_3.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image_4' => 'nullable',
            'image_4.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image_5' => 'nullable',
            'image_5.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        /* Get selected Portfolio */
        $portfolio =  PortfolioPosts::find($id);

        /**
         * Check image if exist
         */
        if ($request->hasFile('image_1')) {

            foreach ($request->file('image_1') as $image) {
                $names = $image->getClientOriginalName();
                $image->move(public_path() . '/uploads/', $names);
                $image1 = $names;
            }
        }

        if ($request->hasFile('image_2')) {

            foreach ($request->file('image_2') as $image) {
                $names = $image->getClientOriginalName();
                $image->move(public_path() . '/uploads/', $names);
                $image2 = $names;
            }
        }

        if ($request->hasFile('image_3')) {

            foreach ($request->file('image_3') as $image) {
                $names = $image->getClientOriginalName();
                $image->move(public_path() . '/uploads/', $names);
                $image3 = $names;
            }
        }

        if ($request->hasFile('image_4')) {

            foreach ($request->file('image_4') as $image) {
                $names = $image->getClientOriginalName();
                $image->move(public_path() . '/uploads/', $names);
                $image4 = $names;
            }
        }

        if ($request->hasFile('image_5')) {

            foreach ($request->file('image_5') as $image) {
                $names = $image->getClientOriginalName();
                $image->move(public_path() . '/uploads/', $names);
                $image5 = $names;
            }
        }



        /**
         * Check image if exist then updating it
         * */
        if (isset($image1)) {
            $portfolio->image_1 = $image1;
        } else {
            # code...
        }

        if (isset($image2)) {
            $portfolio->image_2 = $image2;
        } else {
            # code...
        }

        if (isset($image3)) {
            $portfolio->image_3 = $image3;
        } else {
            # code...
        }

        if (isset($image4)) {
            $portfolio->image_4 = $image4;
        } else {
            # code...
        }

        if (isset($image5)) {
            $portfolio->image_5 = $image5;
        } else {
            # code...
        }

        /* Update Request */
        $portfolio->title = $request->input('title');
        $portfolio->content = $request->input('content');
        $portfolio->save();

        /* Update category of portfolio */
        $portfolio->portcats()->sync($request->input('categories'));

        /* Redirect to portfolio index with successful message */
        return redirect()->route('portfolioposts.index')
            ->with('success', 'Portfolio updated successfully');
    }







    /**
     * Remove Portfolio
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        // Get data from table posts
        $data = PortfolioPosts::find($id);

        // Detach Category Posts
        $data->portcats()->detach();

        // Delete Data
        $data->delete();

        /* Redirect to portfolio index with successful message */
        return redirect()->route('portfolioposts.index')
            ->with('success', 'Portfolio deleted successfully');
    }




}

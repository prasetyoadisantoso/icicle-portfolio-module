<?php

namespace Modules\Portfolio\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Administrator\Http\Controllers\Controller;
use Modules\Administrator\Classes\ReadModuleClass;
use Illuminate\Support\Facades\Auth;
use Modules\Portfolio\Entities\PortfolioCategories;

class PortfolioCategoriesController extends Controller
{

    // roles permissions variable
    public $roles;

    // Read Modules variable
    public $modules;

    public function __construct()
    {
        /* Permission Categories of Portfolios */
        $this->middleware('permission:portfolio-category-list')->only(['index']);
        $this->middleware('permission:portfolio-category-create')->only(['create']);
        $this->middleware('permission:portfolio-category-edit')->only(['edit']);
        $this->middleware('permission:portfolio-category-delete')->only(['destroy']);


        $this->middleware(['auth', 'verified']);
        $this->roles = Auth::user();
        $this->modules = ReadModuleClass::read();
    }






    /**
     * Display a listing of Portfolio's Categories.
     * @return Renderable
     */
    public function index(Request $request)
    {
        /* Get Categories data and show it */
        $data = PortfolioCategories::orderBy('id', 'DESC')->paginate(5);

        /* Render to blade */
        return view('portfolio::categories.index', compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);
    }







    /**
     * Show the form for creating a new Categories.
     * @return Renderable
     */
    public function create()
    {
        /* Go to Create form, no data to render with ;-) */
        return view('portfolio::categories.create');
    }







    /**
     * Store a newly Categories in storage and Database.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        /* Validating request */
        $detail = request()->validate([
            'title' => 'required',
        ]);

        /**
         * Save Category
         */
        $form = new PortfolioCategories();
        $form->title = $detail['title'];
        $form->save();

        /* Redirect to category index */
        return redirect()->route('portfoliocategories.index')
            ->with('success', 'Category created successfully.');
    }






    /**
     * Show the specified resource. Maybe next feature
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('portfolio::show');
    }







    /**
     * Show the form for editing the selected category.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        /* Get selected category */
        $data = PortfolioCategories::find($id);

        /* Render to blade */
        return view('portfolio::categories.edit', compact('data'));
    }








    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        /* Validating request */
        $this->validate($request, [
            'title' => 'required'
        ]);

        /* Update and save category */
        $data = PortfolioCategories::find($id);
        $data->title = $request->input('title');
        $data->save();

        /* Redirect to category index with successful message */
        return redirect()->route('portfoliocategories.index')->with('success', 'Category has been updated');
    }








    /**
     * Remove Category.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        /* Get data of category */
        $data = PortfolioCategories::find($id);

        /* Detach category with portfolio */
        $data->portpost()->detach();

        /* Delete category */
        $data->delete();

        /* Redirect to category index with successful message */
        return redirect()->route('portfoliocategories.index')
            ->with('success', 'Category deleted successfully');
    }
}

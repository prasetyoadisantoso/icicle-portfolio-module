<?php

namespace Modules\Portfolio\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Administrator\Classes\ReadModuleClass;
use Modules\Portfolio\Classes\PortfolioClass;
use Illuminate\Support\Facades\Auth;

class PortfolioController extends Controller
{

    // roles permissions function
    public $roles;

    // Read Modules
    public $modules;

    // Total Portfolio
    public $total_portfolio;

    // Total Categories
    public $total_category;

    // Latest Portfolio
    public $latest_portfolio;

    // Latest Categories
    public $latest_category;


    public function __construct()
    {
        /* Access permissions for Portfolio */
        $this->middleware('permission:portfolio-access');
        $this->middleware(['auth', 'verified']);


        $this->roles = Auth::user();
        $this->modules = ReadModuleClass::read();
        $this->total_portfolio = PortfolioClass::totalPortfolio();
        $this->total_category = PortfolioClass::totalCategories();
        $this->latest_category = PortfolioClass::latestCategories();
        $this->latest_portfolio = PortfolioClass::latestPortfolio();
    }


    /**
     * Dashboard Portfolio
     * @return Renderable
     */
    public function index(Request $request)
    {
        // Share value from AdditionalClass to Portfolio Dashboard
        view()->share('total_portfolio', $this->total_portfolio);
        view()->share('total_category', $this->total_category);
        view()->share('latest_category', $this->latest_category);
        view()->share('latest_portfolio', $this->latest_portfolio);

        /* Go to dashboard Index */
        return view('portfolio::dashboard.index');
    }


}

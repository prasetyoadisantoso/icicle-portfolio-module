<?php
namespace Modules\Portfolio\Classes;

use Illuminate\Support\Facades\View;
use Modules\Portfolio\Entities\PortfolioPosts;
use Modules\Portfolio\Entities\PortfolioCategories;
use Illuminate\Support\Str;

class PortfolioClass {

    // Get Portfolio total
    public static function totalPortfolio()
    {
        $total = PortfolioPosts::get()->count();
        return $total;
    }

    // Get Portfolio's Categories total
    public static function totalCategories()
    {
        $total = PortfolioCategories::get()->count();
        return $total;
    }

    // Get Latest Portfolio
    public static function latestPortfolio()
    {
        $latest_portfolio = PortfolioPosts::orderBy('id','DESC')->get();

        foreach ($latest_portfolio as $item) {
            # code...
            $latestPosts = $item->title;
            return Str::limit($latestPosts, 10);
        }

    }

    // Get Latest Portfolio
    public static function latestCategories()
    {
        $latest_categories = PortfolioCategories::orderBy('id','DESC')->get();

        foreach ($latest_categories as $item) {
            # code...
            $latestCategories = $item->title;
            return Str::limit($latestCategories, 10);
        }

    }

    // Get Latest Portfolio for Front-end
    public static function getPortfolioPosts()
    {
        $frontend_portfolio = PortfolioPosts::with('portcats')->orderBy('id','DESC')->get();
        return $frontend_portfolio;
    }


}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Routing Dasboard Portfolio */
Route::prefix('portfolio')->group(function() {
    Route::get('/', 'PortfolioController@index')->name('portfolio.index');
});


/* Routing to Portfolio */
Route::prefix('portfolio-posts')->group(function() {
    Route::get('/', 'PortfolioPostController@index')->name('portfolioposts.index');
    Route::post('images', 'PortfolioPostController@uploadImage')->name('portfolioposts.image');
    Route::get('create', 'PortfolioPostController@create')->name('portfolioposts.create');
    Route::post('store', 'PortfolioPostController@store')->name('portfolioposts.store');
    Route::get('edit/{id}', 'PortfolioPostController@edit')->name('portfolioposts.edit');
    Route::patch('update/{id}', 'PortfolioPostController@update')->name('portfolioposts.update');
    Route::delete('delete/{id}', 'PortfolioPostController@destroy')->name('portfolioposts.delete');
});


/* Routing to Category of Portfolio */
Route::prefix('portfolio-categories')->group(function() {
    Route::get('/', 'PortfolioCategoriesController@index')->name('portfoliocategories.index');
    Route::post('images', 'PortfolioCategoriesController@uploadImage')->name('portfoliocategories.image');
    Route::get('create', 'PortfolioCategoriesController@create')->name('portfoliocategories.create');
    Route::post('store', 'PortfolioCategoriesController@store')->name('portfoliocategories.store');
    Route::get('edit/{id}', 'PortfolioCategoriesController@edit')->name('portfoliocategories.edit');
    Route::patch('update/{id}', 'PortfolioCategoriesController@update')->name('portfoliocategories.update');
    Route::delete('delete/{id}', 'PortfolioCategoriesController@destroy')->name('portfoliocategories.delete');
});

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioPostcatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_postcats', function (Blueprint $table) {
            $table->unsignedBigInteger('portfolio_posts_id');
            $table->unsignedBigInteger('portfolio_categories_id');

            $table->foreign('portfolio_posts_id')
            ->constrained('portfolio_id_post')
            ->references('id')
            ->on('portfolio_posts')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('portfolio_categories_id')
            ->constrained('portfolio_id_category')
            ->references('id')
            ->on('portfolio_categories')
            ->onDelete('cascade')
            ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio_postcats');
    }
}

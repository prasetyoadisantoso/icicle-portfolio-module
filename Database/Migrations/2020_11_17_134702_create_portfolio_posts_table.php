<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 50);
            $table->text('content');
            $table->string('image_1', 50)->nullable();
            $table->string('image_2', 50)->nullable();
            $table->string('image_3', 50)->nullable();
            $table->string('image_4', 50)->nullable();
            $table->string('image_5', 50)->nullable();
            $table->softDeletes('deleted_at', 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio_posts');
    }
}

<?php

namespace Modules\Portfolio\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class AccessPortfolioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insert list of permissions here
        $permissions = [
            'portfolio-access',
            'portfolio-list',
            'portfolio-create',
            'portfolio-edit',
            'portfolio-delete',
            'portfolio-category-list',
            'portfolio-category-create',
            'portfolio-category-edit',
            'portfolio-category-delete',
         ];


         foreach ($permissions as $permission) {
              Permission::create(['name' => $permission]);
         }
    }
}

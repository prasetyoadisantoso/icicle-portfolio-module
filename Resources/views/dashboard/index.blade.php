@extends('portfolio::index')
@section('dashboard-index')


<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Portfolio Dashboard</h1>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('portfolio.index')}}">Home</a></li>
                    <li class="breadcrumb-item active">Portfolio Dashboard</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->




<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-12 col-xs-12 col-lg-12">
                <div class="jumbotron bg-maroon">
                    <h1 class="display-5">Welcome to Portfolio Section </h1>
                    <p class="lead"> Manage your portfolio image and content whatever you want</p>
                    <hr class="my-4">
                    <p>Show your skill to the world</p>
                </div>
            </div>

            <div class="col-12">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-lg-3">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3>{{$total_portfolio}}</h3>

                                <p>Portfolio</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-tv"></i>
                            </div>
                            <a href="{{route('portfolioposts.index')}}" class="small-box-footer">More info <i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-sm-12 col-xs-12 col-lg-3">
                        <!-- small box -->
                        <div class="small-box bg-teal">
                            <div class="inner">
                                <h3>{{$total_category}}</h3>

                                <p>Categories</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-book"></i>
                            </div>
                            <a href="{{route('portfoliocategories.index')}}" class="small-box-footer">More info <i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-sm-12 col-xs-12 col-lg-3">
                        <!-- small box -->
                        <div class="small-box bg-orange">
                            <div class="inner">
                                <h3>{{$latest_portfolio}}</h3>

                                <p>is your latest portfolio</p>
                            </div>
                            <div class="icon">
                                <i class="far fa-clock"></i>
                            </div>
                            <a href="{{route('portfolioposts.index')}}" class="small-box-footer">More info <i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-sm-12 col-xs-12 col-lg-3">
                        <!-- small box -->
                        <div class="small-box bg-primary">
                            <div class="inner">
                                <h3>{{$latest_category}}</h3>

                                <p>is your latest portfolio's category</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            <a href="{{route('portfoliocategories.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>


            </div>
        </div>
    </div>
</section>

@endsection

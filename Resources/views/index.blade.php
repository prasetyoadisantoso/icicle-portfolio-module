@extends('adminlte.layouts.master')

@section('content-dashboard')
<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-olive navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-sm-inline-block">
            <a href="{{url('/')}}" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-sm-inline-block">
            <a href="{{route('logout')}}" class="nav-link">Logout</a>
        </li>
    </ul>
</nav>
<!-- /.navbar -->

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-olive elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
        <img src="{{asset('assets/images/icicle-logo.png')}}" alt="Icicle" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light ml-3">{{config('app.name')}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('assets/images/user.png')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="" class="d-block">{{$user->name}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                    <a href="{{route('administrator')}}" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>

                <li class="nav-header">Portfolio Modules Installed</li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link text-light">
                        <i class="fas fa-tv mr-2"></i>
                        <p>
                            Portfolio Menu
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('portfolio.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Portfolio Dashboard</p>
                            </a>
                        </li>

                        @can('portfolio-category-list')
                        <li class="nav-item">
                            <a href="{{route('portfoliocategories.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Portfolio Categories</p>
                            </a>
                        </li>
                        @endcan

                        @can('portfolio-list')
                        <li class="nav-item">
                            <a href="{{route('portfolioposts.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Portfolio Posts</p>
                            </a>
                        </li>
                        @endcan
                    </ul>

                </li>

                <div class="dropdown-divider"></div>

                <li class="nav-header">Settings and Cache Management</li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-laptop"></i>
                        <p>
                            Settings
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @can('setting-access')
                        <li class="nav-item">
                            <a href="{{route('settings')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Setting</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('cache')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Cache</p>
                            </a>
                        </li>
                        @endcan
                    </ul>
                </li>



                <li class="nav-header">User and Permission Management</li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            User Management
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @can('user-list')
                        <li class="nav-item">
                            <a href="{{route('users')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Users</p>
                            </a>
                        </li>
                        @endcan
                        @can('role-list')
                        <li class="nav-item">
                            <a href="{{route('roles')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Roles</p>
                            </a>
                        </li>
                        @endcan
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">


    {{-- Start Portfolio Dashboard Section --}}
    @yield('dashboard-index')

    {{-- Start Portfolio Post Section --}}
    {{-- Index --}}
    @yield('portfolio-index')
    {{-- Create --}}
    @yield('portfolio-posts-create')
    {{-- Edit --}}
    @yield('portfolio-posts-edit')

    {{-- Start Portfolio categories Section --}}
    {{-- Index --}}
    @yield('categories-index')
    {{-- Create --}}
    @yield('portfolio-categories-create')
    {{-- Edit --}}
    @yield('portfolio-categories-edit')


</div>


<!-- /.content-wrapper -->
<footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="http://prasetyoadisantoso.com" class="text-success">Pras Web
            Developer</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Module Version</b> | Stable-1.0.0
    </div>
</footer>



{{-- Image Preview --}}
<script>
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#category-img-tag-1').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#category-img-tag-2').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#category-img-tag-3').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL4(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#category-img-tag-4').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL5(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#category-img-tag-5').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image_1").change(function(){
        readURL1(this);
    });

    $("#image_2").change(function(){
        readURL2(this);
    });

    $("#image_3").change(function(){
        readURL3(this);
    });

    $("#image_4").change(function(){
        readURL4(this);
    });

    $("#image_5").change(function(){
        readURL5(this);
    });
</script>
{{-- End Image Preview --}}


@endsection

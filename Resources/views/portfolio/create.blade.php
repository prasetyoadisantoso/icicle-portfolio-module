@extends('portfolio::index')
@section('portfolio-posts-create')



<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Portfolio's Posts Management</h1>
                <small>&nbsp; Create a new post's of portfolio</small>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('portfolioposts.index')}}">Home</a></li>
                    <li class="breadcrumb-item active">Create Portfolio</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->



{{-- Message Error --}}
@if (count($errors) > 0)
<div class="alert alert-danger alert-dismissable mx-5">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif




<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-8 col-lg-8">
                <div class="card">




                    <div class="card-header d-flex">
                        <div class="pull-left">
                            <h3>Create Post's Portfolio</h3>
                        </div>
                    </div>





                    <div class="row">
                        <div class="col-md-12">
                            <div class="card-body">

                                @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                                @endif

                                <form action="{{ route('portfolioposts.store') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="">Title</label>
                                        <input type="text" name="title" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Categories</label><br>
                                        <select multiple class="form-control selectpicker" id="categories"
                                            name="categories[]" style="width: 100%">

                                            @foreach ($category as $item)
                                            <option value="{{$item->id}}">{{$item->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Content</label>
                                        <textarea type="text" name="content" class="form-control"></textarea>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <label>Image 1</label>
                                        <input id="image_1" type="file" class="form-control" name="image_1[]">
                                        <img id="category-img-tag-1" width="200px" />
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <label>Image 2</label>
                                        <input id="image_2" type="file" class="form-control" name="image_2[]">
                                        <img id="category-img-tag-2" width="200px" />
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <label>Image 3</label>
                                        <input id="image_3" type="file" class="form-control" name="image_3[]">
                                        <img id="category-img-tag-3" width="200px" />
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <label>Image 4</label>
                                        <input id="image_4" type="file" class="form-control" name="image_4[]">
                                        <img id="category-img-tag-4" width="200px" />
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <label>Image 5</label>
                                        <input id="image_5" type="file" class="form-control" name="image_5[]">
                                        <img id="category-img-tag-5" width="200px" />
                                    </div>

                                    <hr>

                                    <button type="submit" class="btn btn-success"><i
                                            class="fas fa-save mr-2"></i>Save</button>
                                    <a class="btn btn-secondary" href="{{ route('portfolioposts.index') }}"><i
                                            class="fas fa-undo mr-2"></i> Back</a>
                                </form>

                            </div>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>
</section>

{{-- Multiselect Jquery --}}
<script src="{{asset('assets/AdminLTE/js/jquery-2.0.2.min.js')}}"></script>
<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.selectpicker').select2();
    });
</script>


@endsection

@extends('portfolio::index')
@section('portfolio-categories-edit')



<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Categories Management</h1>
                <small>&nbsp; Edit portfolio's categories</small>
                </h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('portfoliocategories.index')}}">Home</a></li>
                    <li class="breadcrumb-item active">Edit Portfolio's Categories</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->



{{-- Message Error --}}
@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif



<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
                <div class="card">


                    <div class="card-header d-flex">
                        <div class="pull-left">
                            <h3>Edit Portfolio's Categories</h3>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-12">

                            <div class="card-body">
                                @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                                @endif
                                <form action="{{ url('portfolio-categories/update/'. $data->id) }}" method="post">
                                    @method('PATCH')
                                    @csrf
                                    <div class="form-group">
                                        <label for="">Category</label>
                                        <input type="text" name="title" class="form-control"
                                            value="{{ $data->title }}">
                                    </div>
                                    <button type="submit" class="btn btn-success"><i
                                            class="fas fa-save mr-2"></i>Save</button>
                                    <a class="btn btn-secondary" href="{{ route('portfoliocategories.index') }}"><i
                                            class="fas fa-undo mr-2"></i> Back</a>
                                </form>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</section>



@endsection

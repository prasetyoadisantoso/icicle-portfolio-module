<?php

namespace Modules\Portfolio\Entities;

use Illuminate\Database\Eloquent\Model;

class PortfolioPosts extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['title', 'content', 'image_1', 'image_2', 'image_3', 'image_4', 'image_5'];

    public function portcats()
    {
        # many to many to portfolio posts
         return $this->belongsToMany('Modules\Portfolio\Entities\PortfolioCategories', 'portfolio_postcats');
    }
}

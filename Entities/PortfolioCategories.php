<?php

namespace Modules\Portfolio\Entities;

use Illuminate\Database\Eloquent\Model;

class PortfolioCategories extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['title'];

    public function portpost()
    {
        # many to many with portfolio posts
        return $this->belongsToMany('Modules\Portfolio\Entities\PortfolioPosts', 'portfolio_postcats');
    }

}
